import keras
import pickle
from videotest import VideoTest
import os
from ssd import SSD300 as SSD
import os.path

input_shape = (300,300,3)

# Change this if you run with other classes than VOC
class_names = ["background", "hand"]
NUM_CLASSES = len(class_names)
weights_folder = "/home/wolfy/Python/ssd_keras/checkpoints/"
model_weight_files = ["weights3.06-2.80.hdf5","weights3.13-3.40.hdf5","weights_fullnet.04-2.73.hdf5", "weights_fullnet.09-5.82.hdf5", "weights_last_ly.06-2.92.hdf5", "weights_last_ly.14-6.53.hdf5"]
video_out_folder = "/home/wolfy/Python/ssd_keras/processed_videos/"
video_folder = "/home/wolfy/Downloads/hands_videos/"
for weight_file in model_weight_files:
 
    model = SSD(input_shape, num_classes=NUM_CLASSES)

    # Change this path if you want to use your own trained weights
    model.load_weights(weights_folder+weight_file)

    vid_test = VideoTest(class_names, model, input_shape)

    # To test on webcam 0, remove the parameter (or change it to another number
    # to test on that webcam)

    for video in ["1_2015-10-03_20-23-27.mp4"]:#os.listdir(video_folder):
        file_out_name =video_out_folder+"{}_{}.mp4".format(video[:-4], weight_file)
        if os.path.isfile(file_out_name):
            print("skipped {}".format(file_out_name))
            continue
        if video.startswith("."):
            continue
        print("processing {} with {}".format(video_folder+video, weight_file))
        vid_test.run(video_folder+video, file_out_name)



