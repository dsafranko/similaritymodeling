import os
import sys
import numpy as np
import scipy.io
import pickle
from PIL import Image


def to_pascal_format(mat_obj, img_size):
    boxes = []
    img_width, img_height = img_size
    for i in range(len(mat_obj['boxes'][0])):
        rect = mat_obj['boxes'][0][i][0][0]

        points = []
        for i in range(4):
            point = tuple(map(tuple, rect[i]))[0][::-1]
            points.append(point)

        points_x, points_y = zip(*points)
        boxes.append(np.asarray([min(points_x) / img_width, min(points_y) / img_height, max(points_x) / img_width,
                                 max(points_y) / img_height, 1]))

    return np.asarray(boxes)


if __name__ == '__main__':

    objects_dict = {}
    default_path = sys.argv[1]
    for file_name in os.listdir(default_path + "/images"):
        if file_name.startswith("."):
                continue
        file_name = file_name[:-4]
        mat = scipy.io.loadmat(default_path + "/annotations/" + file_name + ".mat")

        im = Image.open(default_path + "/images/" + file_name + ".jpg")
        objects_dict[file_name + ".jpg"] = to_pascal_format(mat, im.size)

    pickle.dump(objects_dict, open(sys.argv[2], 'wb'))
