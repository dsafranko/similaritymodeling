import librosa
import os,sys
import soundfile as sf
import numpy as np
from sklearn.svm import SVC
import random
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt

groundTruthFile = sys.argv[1]
audioPath = sys.argv[2]

with open(groundTruthFile) as f:
	content = f.readlines()
        content = [x.strip() for x in content]

groundTruth = []
for ind, line in enumerate(content):
	jumps = (content[ind]).split(' ')
	jumps.pop(0)
	groundTruth.append(map(int, jumps))

files = os.listdir(audioPath)

audioFiles = []
for af in files:
        if "audio" in af:
               audioFiles.append(af)
files = audioFiles
print(files)

blockSize = 24000#samples
frame_duration = 3#seconds
rate=8000.0
rms = []
zero_crossings = []
mfccs = []

X = []
Y = []

for ind, fileName in enumerate(files):
        jumps = groundTruth[ind]
		
        name = fileName.split('.')[0]
        if "audio" in name:
                newFileName = audioPath + name + ".wav"
                print(newFileName)
                block_gen = sf.blocks(newFileName, blocksize=blockSize, overlap = 0)
	
                #get mfcc for frame: mfcc[:,index]
                rmse = []
                mfcc = []
                zeroCrossings = []
                for ind,bl in enumerate(block_gen):
                        bl = bl[:,0]
                        r = librosa.feature.rmse(y=bl, frame_length = blockSize, hop_length = blockSize+1)#+1 to only get this block
                        m = librosa.feature.mfcc(y=bl,sr=rate,hop_length = blockSize + 1, n_fft=blockSize)
                        z = librosa.feature.zero_crossing_rate(y = bl, frame_length = blockSize, hop_length = blockSize + 1)
                
                        rmse.append(r[0])
                        mfcc.append(m[:,0])
                        zeroCrossings.append(z[0])

                mfccs.append(mfcc)
                rms.append(rmse)
                zero_crossings.append(zeroCrossings)
        

print('features extracted')
scaler = StandardScaler()

X_train = []
Y_train = []
X_test = []
Y_test = []
Y = []

allFeatures = []
featureVectors = []
for ind, fileName in enumerate(files):
    time = 0

    chunks = len(rms[ind])
    jumps = groundTruth[ind]
    
    fv = []
    for i in range(chunks):
        r = rms[ind][i]
        zc = zero_crossings[ind][i]
        mf = mfccs[ind][i]
        feature_vector = np.append(r, zc)
        feature_vector = np.append(feature_vector,mf)
        fv.append(feature_vector)
    featureVectors.append(fv)
    allFeatures.extend(fv)
    
print('feature vectors created')
scaler.fit(allFeatures)

for ind in range(len(featureVectors)):
    featureVectors[ind] = scaler.transform(featureVectors[ind])
    
print('data scaled')
shuff = []
for i in range(len(groundTruth)):
    if not len(groundTruth[i]) == 0:
        shuff.append(i)
        
random.shuffle(shuff)
shuff = shuff[0:35]

for ind, fileName in enumerate(files):
    time = 0

    chunks = len(rms[ind])
    jumps = groundTruth[ind]
    if ind in shuff:
        #train
        for i in range(chunks):
            feature_vector = featureVectors[ind][i]
            X_train.append(feature_vector)
            jumpOccured = False
            for jump in jumps:
                if time < jump <= (time + frame_duration):
                    jumpOccured = True
                    break
            if jumpOccured:
                Y_train.append(1)
                Y.append(1)
            else:
                Y_train.append(0)
                Y.append(0)
            time = time + frame_duration
    else:
        #test
        for i in range(chunks):
            feature_vector = featureVectors[ind][i]
            X_test.append(feature_vector)
            jumpOccured = False
            for jump in jumps:
                if time < jump <= (time + frame_duration):
                    jumpOccured = True
                    break
            if jumpOccured:
                Y_test.append(1)
                Y.append(1)
            else:
                Y_test.append(0)
                Y.append(0)
            time = time + frame_duration
        
print('train and test data created')

def checkIfEvent(tm, events):
    for t in range(tm - 3, tm + 4):
        if t in events:
            return True
    return False

clf = AdaBoostClassifier(DecisionTreeClassifier(max_depth=15),
                     algorithm="SAMME",
                     n_estimators=200)

clf.fit(X_train,Y_train)
print('model trained')

TP = 0
FP = 0
FN = 0

classifierPredictions = []
for ind, fileName in enumerate(files):
    jumps = groundTruth[ind]
    detectedJumps = 0

    name = fileName.split('.')[0]
    newFileName = audioPath + name + ".wav"

    time = frame_duration

    out = ""
    chunks = len(rms[ind])
    for i in range(chunks):
        feature_vector = featureVectors[ind][i]
        prediction = clf.predict([feature_vector])
        classifierPredictions.append(prediction)
        if prediction == 1:
            if checkIfEvent(time, jumps):
                TP = TP + 1
                detectedJumps = detectedJumps + 1
            else:
                FP = FP + 1
            out = out + str(time) + " "
        time = time + frame_duration
    FN = FN + (len(jumps) - detectedJumps)
    
print("True positives: " + str(TP)) 
print("False positives: " + str(FP))
print("False negatives: " + str(FN))

fpr, tpr, _ = roc_curve(Y, classifierPredictions)
roc_auc = auc(fpr, tpr)
plt.figure()
lw = 2
plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.show()
