import subprocess
import os,sys

path1 = sys.argv[1]
path2 = sys.argv[2]
path3 = sys.argv[3]
output = sys.argv[4]

for path in [path1, path2, path3]:
	files = os.listdir(path)
	for ind, fileName in enumerate(files):
		if not fileName.endswith('.mp4'):
			continue
	
		name = fileName.split('.')[0]
		newFileName = name + "audio.wav"
		newPath = output + newFileName
		command = "ffmpeg -i " + path + "/" + fileName + " -ac 2 -f wav " + newPath + " 2>/dev/null"
		subprocess.call(command, shell=True)
		print(newFileName + ' extracted')

