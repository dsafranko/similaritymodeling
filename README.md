This repository is created as a part of course 188.501 and 188.498 on TU Wien. 
There are three parts:
 - detection of jump in videos using sound
 - detection of hand position in video
 - detection of winch sound in video
 For more detailed report see Report.pdf 
Detection of hand is done using  SSD: Single Shot MultiBox Detector implementation in Keras from https://github.com/rykov8/ssd_keras. 
Implementation is adopted to be used with Hand dataset from http://www.robots.ox.ac.uk/~vgg/data/hands/. 
Results can be found in this folder: https://drive.google.com/drive/folders/1M0fFH5wr85yXi5zKE9rrRKfY7eQaWAyF?usp=sharing